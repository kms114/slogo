# SLogo

Matthew Faw, George Bernard, Kayla Schultz, Hannah Fuchshuber

## Time Spent

#### Date Started: 

10/14/2016

#### Date Ended:

11/2/2016

#### Hours Spent:

Over 200

## Roles

Matthew Faw - backend, AST, Nodes
George Bernard - frontend 
Kayla Schultz - frontend
Hannah Fuchshuber - backend, Text Parser, Commands

## Resources
Used Java APIs

## Testing Material
Used Eclipse debugger

## Data required by the project
All in file: src.resources 
All the different resource files are crucial to running the program

## Information about using the Program
Pretty straight forward. We implemented mostly everything, although we did not implement recursion. So of the front-to-back communication might be buggy. But everything else should function.
Note that we did not implement the askwith command, nor did we implement grouping ().
Note that the syntax for creating a custom command is as follows:
to :derp [ :a :b :c ] [ setxy :a :b fd :c bk * :b :c ]  derp \[ 1 2 3 \]
(the brackets are required unless the number of inputs is 1).

Additionally, when a user types 
with [ 5 ], 
turtles with ids 1-5 will be created (if they don't already exist), and turtle 5 will be set as the active turtle.
A similar result occurs with ask.

Note that, with our current design, the recursive tree should be constructed correctly.  Things go wrong after the lowest recursive layer branch
is evaluated, since each command branch is occupied at the same spot in memory and thus will all be marked as visited.  If we could modify the
method map to return a new copy of the command branch each time we add it to the tree, then recursion would work like a charm!


## Bugs
Askwith[] was not implemented, so don't try it! Also updating the background color information in the top left of the screen will not actually update the background color (it was working at some point... I think we can go back into the commits and find where). 
There also seems to be a small bug when trying to turn multiple turtles right. The bug only occurs for right turns and not left, so it's very likely just a small calulation error in the Right command class.

Note that we did not implement the toroidal map on the screen. Additionally, when languages are changed, the backend can interpret different languages just fine,
but the GUI text does not actually change languages, unfortunately.

## Extra Features
We implemented nested scopes, not just scopes for different functions. Also the command fd id will move each turtle differently depending on their specific id.

## Impressions

## Things George Bernard Has Written or Sufficiently Messed With (For Rhondu)
It's easier to go by package than by class specifically. 
I wrote: 
+ Every Acceptor Interface
+ All the Sender Interfaces
+ Half of the ApplicationController and ApplicationScene
+ Error Viewing Module, ImageColorModule, TextEditor, Toolbar, 1/2 TurtleBox, TurtleState, and IViewModule interface
+ Language Switcher Interface + enum
+ 1/4 observer relationships
+ All of Router
